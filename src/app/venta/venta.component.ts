import { Component, OnInit } from '@angular/core';
import { ListaDeProductosService } from '../lista-de-productos.service';
import { Producto } from '../Producto';
import { Venta } from '../Venta';
import { VentasService } from '../ventas.service';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.css']
})
export class VentaComponent implements OnInit {
  venta: Venta[] = [];
  listaParaLaVenta: Producto[];
  productoSeleccionado: Producto;
  cantidad = 1;
  flagDeCategoria = false;
  total = 0;
  categoriaSeleccionada: string;

  constructor(private catalogo: ListaDeProductosService, private servicioDeVentas: VentasService) { }

  ngOnInit() { }

  seleccionarProducto(unProducto: Producto) {
    this.productoSeleccionado = unProducto;
  }

  vender() {
    this.servicioDeVentas.agregarVenta({producto: this.productoSeleccionado, cantidad: this.cantidad, fecha: new Date()});
    this.venta.push({producto: this.productoSeleccionado, cantidad: this.cantidad, fecha: new Date()});
    this.catalogo.vender(this.productoSeleccionado, this.cantidad);
    this.total += this.productoSeleccionado.precio * this.cantidad;
  }
  cerrarCompra() {
    this.total = 0;
    this.flagDeCategoria = false;
    this.categoriaSeleccionada = '';
    this.listaParaLaVenta = [];
    this.productoSeleccionado = null;
    this.venta = [];
  }
  cancelar() {
    this.flagDeCategoria = false;
    this.categoriaSeleccionada = '';
    this.productoSeleccionado = null;
    this.listaParaLaVenta = [];
  }
  expandir(categoria: string) {
    this.listaParaLaVenta = [];
    this.categoriaSeleccionada = categoria;
    this.flagDeCategoria = true;
    this.catalogo.getItems().forEach(prodcto => {
      if ( this.catalogo.category(prodcto.categoria) === categoria ) {
        this.listaParaLaVenta.push(prodcto);
      }
    });
  }
}
