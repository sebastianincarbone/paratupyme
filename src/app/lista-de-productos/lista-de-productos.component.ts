import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { ListaDeProductosDataSource } from './lista-de-productos-datasource';
import { ListaDeProductosService } from '../lista-de-productos.service';
import { MensajesService } from '../mensajes.service';

@Component({
  selector: 'app-lista-de-productos',
  templateUrl: './lista-de-productos.component.html',
  styleUrls: ['./lista-de-productos.component.css']
})
export class ListaDeProductosComponent implements OnInit{
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private listaDeProductosService: ListaDeProductosService) {}

  dataSource: ListaDeProductosDataSource;

  displayedColumns = ['id', 'categoria', 'nombre', 'precio', 'ventas', 'inventario'];

  ngOnInit() {
    this.dataSource = new ListaDeProductosDataSource(this.paginator, this.sort, this.listaDeProductosService);
  }
}
