import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';
import { Producto } from '../Producto';
import { ListaDeProductosService } from '../lista-de-productos.service';


/**
 * Data source for the EjemploDeTabla view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class ListaDeProductosDataSource extends DataSource<Producto> {
  data: Producto[];


  constructor(private paginator: MatPaginator, private sort: MatSort, private listaDeProductos: ListaDeProductosService) {
    super();
    this.data = listaDeProductos.getItems();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<Producto[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    // Set the paginators length
    this.paginator.length = this.data.length;

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: Producto[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: Producto[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
        const isAsc = this.sort.direction === 'asc';
        switch (this.sort.active) {
          case 'nombre': return compare(a.nombre, b.nombre, isAsc);
          case 'ventas': return compare(+a.ventas, +b.ventas, isAsc);
          case 'inventario': return compare(+a.inventario, +b.inventario, isAsc);
          case 'precio': return compare(+a.precio, +b.precio, isAsc);
          case 'categoria': return compare(+a.categoria, +b.categoria, isAsc);
          case 'id': return compare(+a.id, +b.id, isAsc);
          default: return 0;
        }
    });
  }
}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
