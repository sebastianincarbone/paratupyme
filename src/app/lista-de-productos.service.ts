import {Injectable, OnInit} from '@angular/core';
import { Producto } from './Producto';
import { MensajesService } from './mensajes.service';

const ListaDeProductos: Producto[] = [
  { id: 1, nombre: 'Ravioles de ricota', precio: 100, ventas: 0, inventario: 0, categoria: 0 },
  { id: 2, nombre: 'Ravioles de ricota y jamon', precio: 100, ventas: 0, inventario: 0, categoria: 0 },
  { id: 3, nombre: 'Ravioles de ricota y verdura', precio: 100, ventas: 0, inventario: 0, categoria: 0 },
  { id: 4, nombre: 'Ravioles de verdura', precio: 100, ventas: 0, inventario: 0, categoria: 0 },
  { id: 5, nombre: 'Ravioles de verdura y pollo', precio: 100, ventas: 0, inventario: 0, categoria: 0 },
  { id: 6, nombre: 'Ravioles de pollo y jamon', precio: 100, ventas: 0, inventario: 0, categoria: 0 },
  { id: 7, nombre: 'Ravioles de campoVerde', precio: 100, ventas: 0, inventario: 0, categoria: 0 },
  { id: 8, nombre: 'Sorrentinos', precio: 100, ventas: 0, inventario: 0, categoria: 0 },
  { id: 9, nombre: 'Panzotis', precio: 100, ventas: 0, inventario: 0, categoria: 0 },
  { id: 10, nombre: 'Queso100gr', precio: 100, ventas: 0, inventario: 0, categoria: 1 }
];

@Injectable({
  providedIn: 'root'
})

export class ListaDeProductosService {

    catalogo: Producto[] = ListaDeProductos;
    listado: string[] = ['ravioles', 'otros'];
    constructor(private mensaje: MensajesService) {}

    getItems() {
      // this.mensaje.agregar('se entrego una lista de productos');
      return this.catalogo;
    }

    addItem(unProducto: Producto) {
      // this.mensaje.agregar('se agrego un producto a la lista');
      this.mensaje.agregar(unProducto.categoria.toString());
      this.catalogo.push(unProducto);
    }

    vender(unProducto: Producto, cantidad: number) {
      this.catalogo[unProducto.id - 1].ventas += cantidad;
      this.catalogo[unProducto.id - 1].inventario -= cantidad;
    }

    addCategory(category: string) {
      this.listado.push( category );
    }

    indexOfCategory(category: string): number {
      return this.listado.indexOf(category);
    }

    category(numeroDeCategoria: number): string {
      return this.listado[numeroDeCategoria];
    }
    addStockOf(unaCantidad: number, unProducto: Producto) {
      this.catalogo[unProducto.id - 1].inventario += unaCantidad;
    }
}
