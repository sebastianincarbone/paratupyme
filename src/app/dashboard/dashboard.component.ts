import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointState, BreakpointObserver } from '@angular/cdk/layout';
import { VentasService } from '../ventas.service';
import { Producto } from '../Producto';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  productosMasVendidos: Producto[];

  constructor(private breakpointObserver: BreakpointObserver, private servicioDeVentas: VentasService) {}

  ngOnInit(): void {
    this.productosMasVendidos = this.servicioDeVentas.losXProductosMasVendidos(5);
  }

}
