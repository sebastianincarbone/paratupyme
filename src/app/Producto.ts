export interface Producto {
    id: number;
    nombre: string;
    precio: number;
    ventas: number;
    inventario: number;
    categoria: number;
}
