import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MensajesService {

  mensajes: String[] = [];

  agregar(arg0: string): any {
    this.mensajes.push(arg0);
  }

  borrarTodo() {
    this.mensajes = [];
  }

  borrar() {
    this.mensajes.shift();
  }
  constructor() { }
}
