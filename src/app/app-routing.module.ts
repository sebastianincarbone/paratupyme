import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ListaDeProductosComponent } from './lista-de-productos/lista-de-productos.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CrearProductoComponent } from './crear-producto/crear-producto.component';
import { VentaComponent } from './venta/venta.component';
import { StockComponent } from './stock/stock.component';

const routes: Routes = [
  { path: '', redirectTo: '/venta', pathMatch: 'full' },
  { path: 'stock', component: StockComponent },
  { path: 'listaDeProductos', component: ListaDeProductosComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'crearProducto', component: CrearProductoComponent },
  { path: 'venta', component: VentaComponent }
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
