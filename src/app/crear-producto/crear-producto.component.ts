import { Component, OnInit } from '@angular/core';
import { Producto } from '../Producto';
import {ListaDeProductosService} from '../lista-de-productos.service';
import { throwIfEmpty } from 'rxjs/operators';

@Component({
  selector: 'app-crear-producto',
  templateUrl: './crear-producto.component.html',
  styleUrls: ['./crear-producto.component.css']
})

export class CrearProductoComponent implements OnInit {
  id: number = (this.catalogo.catalogo.length + 1);
  categoria: number;
  nuevaCategoria: string;
  flagDeCategoriaNueva = false;
  producto: Producto = { id: this.id, nombre: 'nombreDelProducto', precio: 0, ventas: 0, inventario: 0, categoria: this.categoria };

  constructor(private catalogo: ListaDeProductosService) { }

  ngOnInit() {}

  public agregarUnaCategoria(categoria: string) {
    this.catalogo.addCategory(categoria);
    this.flagDeCategoriaNueva = false;
    this.nuevaCategoria = '';
  }
  public crearNuevaCategoria() {
    this.flagDeCategoriaNueva = true;
  }
  public agregarProducto() {
    this.producto = { id: this.id, nombre: 'nombreDelProducto', precio: 0, ventas: 0, inventario: 0, categoria: this.categoria };
    this.catalogo.addItem(this.producto);
    this.borrarFormulario();
  }
  public selectCategory(categoria: number) {
    this.categoria = categoria;
  }
  borrarFormulario() {
    this.producto = {id: this.catalogo.catalogo.length + 1, nombre: 'nombreDelProducto', precio: 0, ventas: 0, inventario: 0, categoria: 0};
  }
}
