import { Component, OnInit } from '@angular/core';
import { ListaDeProductosService } from '../lista-de-productos.service';
import { Producto } from '../Producto';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {
  flagDeCategoria: boolean;
  categoriaSeleccionada: string;
  productoSeleccionado: Producto;
  listaParaLaVenta: Producto[];
  cantidad = 0;
  constructor(private inventario: ListaDeProductosService) { }

  ngOnInit() {
  }

  seleccionarProducto(unProducto: Producto) {
    this.productoSeleccionado = unProducto;
  }

  agregarXCantidadDeUnProducto(unaCantidad: number, unProducto: Producto) {
    this.inventario.addStockOf(unaCantidad, unProducto);
    this.cantidad = 0;
  }

  cancelar() {
    this.flagDeCategoria = false;
    this.categoriaSeleccionada = '';
    this.productoSeleccionado = null;
    this.listaParaLaVenta = [];
    this.cantidad = 0;
  }
  expandir(categoria: string) {
    this.listaParaLaVenta = [];
    this.categoriaSeleccionada = categoria;
    if (this.flagDeCategoria) { this.categoriaSeleccionada = ''; }
    this.flagDeCategoria = !this.flagDeCategoria;
    this.inventario.getItems().forEach(prodcto => {
      if ( this.inventario.category(prodcto.categoria) === categoria ) {
        this.listaParaLaVenta.push(prodcto);
      }
    });
  }
}
