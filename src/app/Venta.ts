import { Producto } from './Producto';

export interface Venta {
    producto: Producto;
    cantidad: number;
    fecha: Date;
}
