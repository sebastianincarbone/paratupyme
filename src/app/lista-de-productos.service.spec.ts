import { TestBed, inject } from '@angular/core/testing';

import { ListaDeProductosService } from './lista-de-productos.service';

describe('ListaDeProductosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListaDeProductosService]
    });
  });

  it('should be created', inject([ListaDeProductosService], (service: ListaDeProductosService) => {
    expect(service).toBeTruthy();
  }));
});
