import { Injectable } from '@angular/core';
import { Venta } from './Venta';
import { Producto } from './Producto';
import { ListaDeProductosService } from './lista-de-productos.service';

@Injectable({
  providedIn: 'root'
})
export class VentasService {

  ventas: Venta[] = [];
  productosMasVendidos: Set<Producto>;
  ventasDelDia = 0;

  constructor(private servicioDeCatalogo: ListaDeProductosService) { }

  agregarVenta(unaVenta: Venta) {
    this.ventas.push(unaVenta);
  }
  calcularVentasDelDia(): number {
    this.ventasDelDia = 0;
    this.ventas.forEach(unaVenta => {
      // ver como conseguir el dia de hoy.
        this.ventasDelDia += (unaVenta.producto.precio * unaVenta.cantidad);
    });

    return this.ventasDelDia;
  }
  losXProductosMasVendidos( unaCantidadDeProductos: number ): any {
    this.productosMasVendidos = new Set<Producto>();
    this.ventas.forEach( unaVenta => {
      this.productosMasVendidos.add(unaVenta.producto);
    });
    return this.productosMasVendidos;
  }

}

